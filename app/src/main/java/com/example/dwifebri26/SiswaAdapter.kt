package com.example.dwifebri26

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView

class SiswaAdapter(private val data: ArrayList<Siswa>?): RecyclerView.Adapter<SiswaAdapter.SiswaViewHolder>() {

    class SiswaViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val nama = itemView.findViewById<TextView>(R.id.namaSiswa)
        private val nis = itemView.findViewById<TextView>(R.id.nisSiswa)
        private val Editbtn = itemView.findViewById<Button>(R.id.btnEdit)
        private val Hapusbtn = itemView.findViewById<Button>(R.id.btnHapus)
        fun bind(get: Siswa) {
            nama.text = get.nama
            nis.text = get.nis

            Editbtn.setOnClickListener() {
                val intent = Intent(itemView.context, edit_activity::class.java)
                intent.putExtra("id", get.id)
                intent.putExtra("nama", get.nama)
                intent.putExtra("nis", get.nis)
                itemView.context.startActivity(intent)
            }

            Hapusbtn.setOnClickListener() {
                val dialogBuilder = AlertDialog.Builder(itemView.context)
                dialogBuilder.setTitle("Hapus Data")
                dialogBuilder.setMessage("Hapus siswa" + get.nama)
                dialogBuilder.setPositiveButton("Delete", DialogInterface.OnClickListener { _, _ ->
                    val db = DBHelper(itemView.context, null)
                    val status = db.deleteSiswa(get.id)
                    if (status > -1) Toast.makeText(
                        itemView.context,
                        "data dihapus",
                        Toast.LENGTH_LONG
                    ).show()
                })
                dialogBuilder.setNegativeButton("Cancel", DialogInterface.OnClickListener { _, _ ->

                })
                dialogBuilder.create()
                dialogBuilder.show()
            }

        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SiswaViewHolder {
        return SiswaViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_siswa, parent, false))
    }

    override fun onBindViewHolder(holder: SiswaViewHolder, position: Int) {
        holder.bind(data?.get(position) ?: Siswa("", "", ""))

    }
    override fun getItemCount(): Int {
            return data?.size ?: 0
        }
    }
